import os

import uvicorn
from fastapi.templating import Jinja2Templates
from fastapi.websockets import WebSocket
from fastapi.concurrency import run_until_first_complete
from fastapi import FastAPI, Request
from .broadcaster import Broadcast
import json
import logging
from typing import List
from .database import create_connection, add_new_channel, delete_existing_channel, get_channels_list

BROADCAST_URL = os.environ.get("BROADCAST_URL", "memory://")

broadcast = Broadcast(BROADCAST_URL)
templates = Jinja2Templates("backend/templates")

app = FastAPI(on_startup=[broadcast.connect], on_shutdown=[broadcast.disconnect])

db_connection = create_connection("websockets.db")


@app.get("/")
async def homepage(request: Request):
    template = "index.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)


@app.post("/channel")
async def new_channel(channel_name: str):
    logging.info(f"Creating new channel: {channel_name}")
    add_new_channel(db_connection, channel_name)


@app.delete("/channel")
async def delete_channel(channel_name: str):
    logging.info(f"Deleting channel: {channel_name}")
    delete_existing_channel(db_connection, channel_name)


@app.get("/channel")
async def channels_list() -> List[str]:
    return get_channels_list(db_connection)


@app.websocket_route("/chatroom_ws")
async def chatroom_ws(ws: WebSocket):
    await ws.accept()
    await run_until_first_complete(
        (chatroom_ws_receiver, {"ws": ws}),
        (chatroom_ws_sender, {"ws": ws}),
    )


async def chatroom_ws_receiver(ws: WebSocket):
    async for message in ws.iter_text():
        message_obj = json.loads(message)
        if message_obj["action"] == "message":
            await broadcast.publish(channel="chatroom", message=message)
        if message_obj["action"] == "channel_subscribe":
            logging.info(
                f"{message_obj['session_id']} subscribed to channel {message_obj['channel']}"
            )


async def chatroom_ws_sender(ws: WebSocket):
    async with broadcast.subscribe(channel="chatroom") as subscriber:
        async for event in subscriber:
            await ws.send_text(event.message)
