import sqlite3
import logging
from contextlib import closing
from typing import List


CHANNELS_TABLE = "channels"


def create_connection(db_file):
    """create a database connection to a SQLite database"""
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        logging.info(sqlite3.version)
        with closing(conn.cursor()) as cursor:
            cursor.execute(f"CREATE TABLE IF NOT EXISTS {CHANNELS_TABLE} (name TEXT);")
    except sqlite3.Error as e:
        logging.error(e)
    finally:
        if conn:
            conn.close()


def get_channels_list(db_connection: sqlite3.Connection) -> List[str]:
    with closing(db_connection.cursor()) as cursor:
        channel_names = cursor.execute(f"SELECT name FROM {CHANNELS_TABLE}").fetchall()
    return channel_names


def add_new_channel(db_connection: sqlite3.Connection, new_channel_name: str):
    current_channels = get_channels_list(db_connection)
    if new_channel_name not in current_channels:
        with closing(db_connection.cursor()) as cursor:
            cursor.execute(
                f"INSERT INTO {CHANNELS_TABLE} (name) VALUES ('{new_channel_name}')"
            )


def delete_existing_channel(db_connection: sqlite3.Connection, channel_name: str):
    with closing(db_connection.cursor()) as cursor:
        cursor.execute(f"DELETE FROM {CHANNELS_TABLE} WHERE name='{channel_name}'")
